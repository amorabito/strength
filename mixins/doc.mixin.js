import httpMixin from '~/mixins/http.mixin'

export default {
  mixins: [httpMixin],
  data: () => ({
    local: null
  }),
  created() {
    this.local = this.$db
  },
  methods: {
    getAllDocs() {
      return this.local
        .allDocs({ include_docs: true, descending: true })
        .then(({ rows }) => rows.map((r) => r.doc))
    },
    findDocIdsLike(docId, sort) {
      // NOTE: this can cause issues when string contains regex special values
      // the '|' character in google oauth userId acted as an OR for the query
      // causing the exercises / active workout to come with the response
      const escapedDocId = docId.replace(/\|/g, '\\|')
      return this.local
        .find({
          selector: {
            _id: {
              $regex: escapedDocId
            }
          },
          sort: sort || [{ _id: 'desc' }]
        })
        .then((body) => body.docs)
    },
    getDoc(docId) {
      return this.local.get(docId)
    },
    saveDoc(doc) {
      return this.local.put(doc).then(({ id, rev }) => {
        return { ...doc, _id: id, _rev: rev }
      })
    },
    deleteDoc(doc) {
      return this.local.remove(doc._id, doc._rev)
    }
  }
}
