import docMixin from '~/mixins/doc.mixin'

const pouchCollate = require('pouchdb-collate')

export default {
  mixins: [docMixin],
  data: () => ({
    exerciseTypes: [
      'Barbell',
      'Dumbbell',
      'Machine/Other',
      'Weighted bodyweight',
      'Assisted bodyweight',
      'Reps Only',
      'Cardio',
      'Duration'
    ],
    setTypes: ['Normal', 'Warm up', 'Failure', 'Drop set']
  }),
  methods: {
    isWeightedExercise(exerciseType) {
      return [0, 1, 2].includes(
        this.exerciseTypes.findIndex((t) => t === exerciseType)
      )
    },
    saveExercise(exercise) {
      if (!exercise._id) {
        const userId = this.$auth.getUserId()
        exercise._id = pouchCollate.toIndexableString([
          userId,
          'Exercise',
          exercise.name
        ])
      }
      return this.saveDoc(exercise)
    },
    getExercises() {
      return this.findDocIdsLike(`Exercise`, ['_id'])
    },
    getExercise(id) {
      return this.getDoc(id)
    },
    getMyExercises() {
      const userId = this.$auth.getUserId()
      return this.findDocIdsLike(`${userId}.+Exercise`, ['_id'])
    },
    getMyWorkouts(sort) {
      const userId = this.$auth.getUserId()
      return this.findDocIdsLike(`${userId}.+Workout`, sort)
    }
  }
}
